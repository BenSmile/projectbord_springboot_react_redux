package io.ben.projectboard.service;


import io.ben.projectboard.domain.ProjectTask;
import io.ben.projectboard.repository.ProjectTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectTaskService {

    @Autowired
    private ProjectTaskRepository projectTaskRepository;

    public ProjectTask saveOrUpdateProjectTask(ProjectTask projectTask){

        if(projectTask.getStatus() == null || projectTask.getStatus()==""){
            projectTask.setStatus("TO_DO");
        }
        return projectTaskRepository.save(projectTask);
    }

    public Iterable<ProjectTask> findAll(){
        return projectTaskRepository.findAll();
    }

    public ProjectTask findById(  Long id){
        return projectTaskRepository.getById(id);
    }

    public void deletePT(  Long id){
        if(findById(id)!=null){
            projectTaskRepository.delete(findById(id));
        }
    }

}
