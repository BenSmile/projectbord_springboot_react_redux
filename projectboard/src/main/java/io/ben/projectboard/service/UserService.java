package io.ben.projectboard.service;

import io.ben.projectboard.domain.User;
import io.ben.projectboard.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public User createUpdateUser(User user){
        return userRepository.save(user);
    }

    public Iterable<User> findAll(){
        return userRepository.findAll();
    }

    public User findById(Integer id){
        return userRepository.getById(id);
    }

    public User findByName(String name){
        return userRepository.findByName(name);
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public void delete(Integer id){
        if(userRepository.getById(id) == null){
            userRepository.delete(userRepository.getById(id));
        }
    }

    public User login (User u){
       return userRepository.findByEmailAndPassword(u.getEmail(), u.getPassword());
    }
}
