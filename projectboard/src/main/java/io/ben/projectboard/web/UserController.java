package io.ben.projectboard.web;

import io.ben.projectboard.domain.ProjectTask;
import io.ben.projectboard.domain.User;
import io.ben.projectboard.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public Iterable<User> getAll() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserByID(@PathVariable Integer id) {
        User u = userService.findById(id);
        return new ResponseEntity<User>(u, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<?> createUpdateUser(@Valid @RequestBody User user, BindingResult result) {
        User byName = userService.findByName(user.getName());
        System.out.println("byName = " + byName == null);
        User byMail = userService.findByEmail(user.getEmail());
        System.out.println("byMail==null = " + byMail == null);

        if (user.getId()!=null) {
            User u = userService.createUpdateUser(user);
            return new ResponseEntity<User>(u, HttpStatus.OK);
        }

        if (byMail == null && byName == null) {
            User u = userService.createUpdateUser(user);
            return new ResponseEntity<User>(u, HttpStatus.CREATED);
        }
        if (result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();
            for (FieldError error : result.getFieldErrors()) {
                errorMap.put(error.getField(), error.getDefaultMessage());
            }
            return new ResponseEntity(errorMap, HttpStatus.OK);
        }
        return new ResponseEntity("User already exists", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Integer id) {
        userService.delete(id);
        return new ResponseEntity<String>("User deleted", HttpStatus.OK);
    }

    @PostMapping("/login/")
    public ResponseEntity<?> login(@RequestBody User user) {
        User u = userService.login(user);
        if (u == null) {
            return new ResponseEntity<String>("wrong credentials", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<User>(u, HttpStatus.OK);
        }
    }


}
