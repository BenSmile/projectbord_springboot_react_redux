package io.ben.projectboard.web;


import io.ben.projectboard.domain.ProjectTask;
import io.ben.projectboard.service.ProjectTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/board")
@CrossOrigin
public class ProjectTaskController {

    @Autowired
    private ProjectTaskService projectTaskService;


    //    this method work for update and create task
    @PostMapping("")
    public ResponseEntity<?> addPTToBoard(@Valid @RequestBody ProjectTask projectTask, BindingResult result) {
        Map<String, String> errorMap = new HashMap<>();
        if (projectTask.getSummary() == null || projectTask.getSummary() == "") {
            errorMap.put("summary", "Summary cannot be blank");
            return new ResponseEntity(errorMap, HttpStatus.BAD_REQUEST);
        }
//
//        if (result.hasErrors()){
//            for (FieldError error : result.getFieldErrors()) {
//                errorMap.put(error.getField(), error.getDefaultMessage());
//            }
//            return new ResponseEntity(errorMap, HttpStatus.BAD_REQUEST);
//        }
        ProjectTask newPT = projectTaskService.saveOrUpdateProjectTask(projectTask);
        return new ResponseEntity(newPT, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public Iterable<ProjectTask> getAllPTs() {
        return projectTaskService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity getPTById(@PathVariable Long id) {
        ProjectTask byId = projectTaskService.findById(id);
        return new ResponseEntity(byId, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletePT(@PathVariable Long id) {
        projectTaskService.deletePT(id);
        return new ResponseEntity<String>("Project task deleted", HttpStatus.OK);
    }


}
