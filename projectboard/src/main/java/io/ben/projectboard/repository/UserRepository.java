package io.ben.projectboard.repository;

import io.ben.projectboard.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository  extends CrudRepository<User, Integer> {

    User getById(Integer id);

    @Query("select u from User u where u.name=?1")
    User findByName(String name);
    @Query("select u from User u where u.email=?1")
    User findByEmail(String email);

    @Query("select u from User u where u.email=?1 and u.password=?2 and u.disabled=false")
    User findByEmailAndPassword(String email, String password);

}
